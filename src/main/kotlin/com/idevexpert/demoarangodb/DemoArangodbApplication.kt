package com.idevexpert.demoarangodb

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class DemoArangodbApplication

fun main(args: Array<String>) {
    runApplication<DemoArangodbApplication>(*args)
}
