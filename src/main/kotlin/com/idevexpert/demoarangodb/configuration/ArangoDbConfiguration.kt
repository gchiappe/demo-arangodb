package com.idevexpert.demoarangodb.configuration

import com.arangodb.ArangoDB
import com.arangodb.springframework.annotation.EnableArangoRepositories
import com.arangodb.springframework.config.AbstractArangoConfiguration
import org.springframework.context.annotation.Configuration

// Bean
@Configuration
@EnableArangoRepositories("com.idevexpert.demoarangodb.persistence")
class ArangoDbConfiguration : AbstractArangoConfiguration() {

    // Devuelve la conexión de ArangoDB
    override fun arango(): ArangoDB.Builder {
        return ArangoDB.Builder()
                .host("localhost", 32768)
                .user("demodocu")
                .password("123456")
    }

    // Devuelve nombre de la BD
    override fun database(): String {
        return "demodocu"
    }

}