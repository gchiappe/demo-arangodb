package com.idevexpert.demoarangodb.bean.response

data class CreateGameResponse(
        val success: Boolean
)