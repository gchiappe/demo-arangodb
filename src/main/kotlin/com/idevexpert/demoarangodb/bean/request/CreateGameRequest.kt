package com.idevexpert.demoarangodb.bean.request

data class CreateGameRequest(
        val nombre: String,
        val rating: Double
)