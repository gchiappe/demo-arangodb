package com.idevexpert.demoarangodb.controller

import com.idevexpert.demoarangodb.persistence.ExpedienteRepository
import com.idevexpert.demoarangodb.persistence.UsuarioRepository
import com.idevexpert.demoarangodb.persistence.entity.Expediente
import com.idevexpert.demoarangodb.persistence.entity.Usuario
import org.apache.coyote.Response
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import java.time.LocalDateTime

@RestController
class ExpedienteDemoController(
    private val usuarioRepository: UsuarioRepository,
    private val expedienteRepository: ExpedienteRepository
) {


    @PostMapping("/usuario")
    fun crearNuevoUsuario(
        @RequestParam("username") username: String,
        @RequestParam("password") password: String,
        @RequestParam("email") email: String
    ) : ResponseEntity<String> {
        val usuario = Usuario(
            username = username,
            password = password,
            email = email
        )
        val usuarioGuardado = usuarioRepository
            .save(usuario)
        return ResponseEntity(
            usuarioGuardado.id,
            HttpStatus.CREATED
        )
    }

    @GetMapping("/usuario")
    fun obtenerTodosLosUsuarios() : ResponseEntity<List<Usuario>> {
        val usuarios = arrayListOf<Usuario>()

        usuarioRepository.findAll().forEach { usuarios.add(it) }

        return ResponseEntity(
            usuarios,
            HttpStatus.OK
        )
    }

    @GetMapping("/usuario/{id}")
    fun obtenerUsuario(
        @PathVariable id: String
    ) : ResponseEntity<Usuario> {
        return try {
            ResponseEntity(
                usuarioRepository.findById(id).get(),
                HttpStatus.OK
            )
        } catch (_: Exception) {
            ResponseEntity(
                null,
                HttpStatus.NOT_FOUND
            )
        }
    }

    @PostMapping("/usuario/{id}")
    fun editarUsuario(
        @PathVariable id: String,
        @RequestParam("password", defaultValue = "") password: String,
        @RequestParam("email", defaultValue = "") email: String
    ) : ResponseEntity<Usuario> {
        val usuario = try {
                usuarioRepository.findById(id).get()
        } catch (_: Exception) {
            return ResponseEntity(
                null,
                HttpStatus.NOT_FOUND
            )
        }

        if (password.isNotEmpty()) usuario.password = password
        if (email.isNotEmpty()) usuario.email = email
        usuario.fechaDeModificacion = LocalDateTime.now()

        val usuarioGuardado = usuarioRepository.save(usuario)

        return ResponseEntity(
            usuarioGuardado,
            HttpStatus.ACCEPTED
        )
    }

    /**
     *
     * val nroExpediente: String,
        var asunto: String,
        var estado: EstadoDeExpediente = EstadoDeExpediente.BORRADOR,
        @Ref val creador: Usuario,
     */
    @PostMapping("/expediente")
    fun nuevoExpediente(
        @RequestParam nroExpediente: String,
        @RequestParam asunto: String,
        @RequestParam creador: String
    ) : ResponseEntity<String> {
        val usuario = try {
            usuarioRepository.findByUsername(creador)
        } catch (_:Exception) {
            return ResponseEntity("Usuario ${creador} No Existe", HttpStatus.NOT_FOUND)
        }
        val nuevoExpdiente = expedienteRepository.save(
            Expediente(
                asunto = asunto,
                nroExpediente = nroExpediente,
                creador = usuario
            )
        )
        return ResponseEntity(
            nuevoExpdiente.id,
            HttpStatus.CREATED
        )
    }

    @GetMapping("/expediente/porUsuario/{usuario}")
    fun obtenerExpedientesPorUsuario(
        @PathVariable usuario: String
    ) : ResponseEntity<List<Expediente>> {
        val expedientes = try {
            expedienteRepository.findAllByCreador_Username(usuario)
        } catch (_: Exception) {
            return ResponseEntity(null, HttpStatus.NOT_FOUND)
        }
        val listaExpedientes = arrayListOf<Expediente>()
        expedientes.forEach { listaExpedientes.add(it) }
//        if (listaExpedientes.isEmpty()) {
//            return ResponseEntity(null, HttpStatus.)
//        }
        return ResponseEntity(listaExpedientes, HttpStatus.OK)
    }



}