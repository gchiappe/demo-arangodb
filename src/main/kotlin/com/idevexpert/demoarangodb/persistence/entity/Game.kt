package com.idevexpert.demoarangodb.persistence.entity

import com.arangodb.springframework.annotation.Document
import org.springframework.data.annotation.Id
import java.time.LocalDateTime

@Document("games")
data class Game(
        @Id val id: String? = null, // El "?" Significa que PUEDE ser Nulo.

        var nombre: String, // Los que no tienen "?" NUNCA pueden ser Nulo.
        val rating: Double,

        val fechaDeLanzamiento: LocalDateTime = LocalDateTime.now()
)