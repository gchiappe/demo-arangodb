package com.idevexpert.demoarangodb.persistence.entity

import com.arangodb.springframework.annotation.Document
import com.arangodb.springframework.annotation.Ref
import org.springframework.data.annotation.Id
import java.time.LocalDateTime

@Document("expedientes")
data class Expediente(
    @Id val id: String? = null,

    val nroExpediente: String,
    var asunto: String,
    var estado: EstadoDeExpediente = EstadoDeExpediente.BORRADOR,
    @Ref val creador: Usuario,

    val fechaDeCreacion: LocalDateTime = LocalDateTime.now(),
    var fechaDeModificacion: LocalDateTime = LocalDateTime.now()
) {
    enum class EstadoDeExpediente {
        EMITIDO, BORRADOR, DESPACHO, ATENDIDO
    }
}